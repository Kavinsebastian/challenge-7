const {userAdmin, userGame} = require('../models')
const passport = require('../lib/passport');


module.exports = {
    register: (req, res) => {
        const title = req.body.title
        res.render('register', {title})

        // const title = req.body.title
        // res.status(200).json({
        //     status:true,
        //     message:'Hello world'
        // }).render('register', {title})

    },
    registerPost: (req, res, next) => {
        userAdmin.register(req.body)
            .then(() => {
                res.redirect('/login')
            })
            .catch(err => next(err))
    },
    loginPost: passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/login',
        failureFlash: true
    }),
    login: (req, res) => {
        const title = req.body.title
        res.render('login', {title})
    },

    dashboard: (req, res) => {
        const title = 'dashboard'
        const username = req.user.dataValues
        userGame.findAll().then(user => {
            console.log(user);
            res.render('index', {title, user, username, msg: req.flash('msg')})
        })
    },
    usersDelete: (req, res) => {
        const nick = req.params.id;
        userGame.destroy({
            where: {id: nick}
        }).then(user => {
            res.redirect('/dashboard')
        })
    },
    usersUpdate: (req, res) => {
        const username = req.user.dataValues
        const title = 'user update'
        const nick = req.params.id;
        userGame.findOne({
            where: {id: nick}
        }).then(user => {
            res.render('update', {title, user, username})
        })
    },
    usersUpdatePost : (req, res) => {
        const nick = req.params.id;
        userGame.update({
            usernameG: req.body.usernameG,
            email: req.body.email,
            nohp: req.body.nohp
        },
        {
            where: {id: nick}
        }).then(user => {
            req.flash('msg', 'Data User telah Update')
            res.redirect('/dashboard')
        })
    },
    usersDetail: (req, res) => {
        const title = 'user detail';
        const username = req.user.dataValues
        const nick = req.params.id;
        userGame.findOne({
            where: {id : nick}
        }).then(user => {
            // console.log('req.user',req.user);
            res.render('detail', {title, user, username})
        })
    },
    addUser: (req, res) => {
        const title = 'tambah user';
        const username = req.user.dataValues;
        res.render('tambahUser', {title, username})
    },
    addUserPost: (req, res) => {
        userGame.register(req.body)
        .then(() => {
            req.flash('msg', 'Data User telah di tambahkan')
            res.redirect('/dashboard')
        })
    },
    logout: (req, res) => {
        res.cookie('jwt', '', {maxAge: 1})
        res.redirect('/login')
    }
}