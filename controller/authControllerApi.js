const {userGame} = require('../models');


function format(user) {
    const {id, usernameG} = user
    return {
        id,
        usernameG,
        accessToken: user.generateToken()
    }
}

module.exports = {
    register: (req, res, next) => {
        userGame.register(req.body)
        .then(() => {
            res.json({
                message: 'selamat data telah tersimpan'
            });

        }).catch((err) => {
            next(err)
        });
    },
    login: (req, res) => {
        userGame.authenticate(req.body)
        .then(user => {
            res.json(
                format(user)
            )
        })
        .catch(err => res.json({
            message: err
            })
        )
        
    },
    profile: (req, res) => {
        const currentUser = req.user
        res.json(currentUser)
    },
}

