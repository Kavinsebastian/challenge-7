'use strict';
const {
  Model
} = require('sequelize');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class userGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    
 // Method untuk melakukan enkripsi
    static #encrypt = (passwordG) => bcrypt.hashSync(passwordG, 10);

    
// Lalu, kita buat method register
    static register = ({usernameG, passwordG, email, nohp}) => {
      const encryptedPassword = this.#encrypt(passwordG);
    /*
      #encrypt dari static method
      encryptedPassword akan sama dengan string 
      hasil enkripsi password dari method #encrypt
      */

      return this.create({
        usernameG,
        email,
        nohp,
        passwordG: encryptedPassword
      })
    }


/* Semua yang berhubungan dengan login */
 // Method untuk melakukan enkripsi
    checkPassword = passwordG => bcrypt.compareSync(passwordG, this.passwordG);

    /* Method ini kita pakai untuk membuat JWT */
    generateToken = () => {
       // Jangan memasukkan password ke dalam payload
      const payload = {
        id: this.id,
        usernameG: this.usernameG
      }
       // Rahasia ini nantinya kita pakai untuk memverifikasi apakah token ini benar-benar berasal dari aplikasi kita
      const KEY = 'SECRET_KEY';

       // Membuat token dari data-data diatas
      const token = jwt.sign(payload, KEY)
      return token;
    }

/* Method Authenticate, untuk login */
    static authenticate = async({usernameG, passwordG}) => {
      try {
        const user = await this.findOne({where: {usernameG}})
        if(!user) return Promise.reject('user not found')
        const isPasswordValid = user.checkPassword(passwordG);
        if(!isPasswordValid) return Promise.reject('password wrong');

        return Promise.resolve(user)
      } catch (err) {
        return Promise.reject(err)
      }
    }
  };
/* Akhir dari semua yang berhubungan dengan login */


  
  userGame.init({
    usernameG: DataTypes.STRING,
    passwordG: DataTypes.STRING,
    email: DataTypes.STRING,
    nohp: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'userGame',
  });
  return userGame;
};