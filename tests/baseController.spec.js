const base = require('../controller/authController.js')
const mockRequest = (body = {}) => ({body})
const mockResponse = () => {
    const res = {}
    res.json = jest.fn().mockReturnValue(res)
    res.status = jest.fn().mockReturnValue(res)
    res.render = jest.fn().mockReturnValue(res)
    res.redirect = jest.fn().mockReturnValue(res)
    return res
}

describe('base.register function', () => {
    test('res.json called with {status: true, message:"Hello world" }', done => {
        const res = mockResponse()
        const title = 'register'
        const req = mockRequest({title})
        base.register(req, res);
        // expect(res.status).toBeCalledWith(200)
        expect(res.render).toBeCalledWith('register', {title})
        done()
        })
    })

describe('base.registerPost function', () => {
    test('res.redirect("/login")', done => {
        const res = mockResponse()
        const req = mockRequest()
        base.registerPost(req, res);
        expect(res.statusCode).toEqual(200)
        done()
    })
})


describe('base.login function', () => {
    test('res.render called with (login, {title})', done => {
        const title = 'login'
        const res = mockResponse()
        const req = mockRequest({title})
        base.login(req, res)
        expect(res.render).toBeCalledWith('login',{title})
        done()
    })
})