const router = require('express').Router();
const auth = require('../controller/authControllerApi');
const restrict = require('../middleware/restrictApi');

router.post('/api/register',auth.register);

router.post('/api/login', auth.login);

router.get('/api/profile', restrict, auth.profile)

module.exports = router;