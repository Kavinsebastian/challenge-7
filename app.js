var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const session = require('express-session');
const flash = require('express-flash');
const {port = 8000} = process.env
// const router = require('./routes/index');
const passport = require('./lib/passport');
const passportApi = require('./lib/passportJwt');
const routerApi = require('./routes/indexApi');
const auth = require('./controller/authController');
const restrict = require('./middleware/restrict');




var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.use(session({
    secret: 'SECRET_KEY',
    resave: false,
    saveUninitialized:false,
    // cookie: { secure: true } // this line
}));


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize());
app.use(passport.session());
app.use(passportApi.initialize());

app.use(flash());



app.get('/dashboard', restrict, auth.dashboard)
app.get('/register', auth.register)
app.post('/register', auth.registerPost)

app.get('/login',auth.login)
app.post('/login', auth.loginPost)

app.get('/dashboard/delete/:id', auth.usersDelete)
app.get('/dashboard/update/:id', auth.usersUpdate)
app.post('/dashboard/update/:id', auth.usersUpdatePost)


app.get('/dashboard/detail/:id', auth.usersDetail)
app.get('/dashboard/addUser', auth.addUser)
app.post('/dashboard/save', auth.addUserPost)
app.get('/logout', auth.logout)








// app.use(router);
app.use(routerApi);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
app.listen(port, () => console.log(`server berjalan di port ${port}`))

module.exports = app;
