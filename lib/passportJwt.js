const passport = require('passport');
const {Strategy: JWTStrategy, ExtractJwt} = require('passport-jwt');

const {userGame} = require('../models');

const options = {
    jwtFromRequest : ExtractJwt .fromHeader ('authorization' ),
    
    secretOrKey: 'SECRET_KEY'
}

passport.use(new JWTStrategy(options, async(payload, done) => {
    userGame.findByPk(payload.id)
    .then((user) => {
        done(null, user)
    }).catch((err) => {
        done(err, false)
    });
}))

module.exports = passport;