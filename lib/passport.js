const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const {userAdmin} = require('../models');

async function authenticate(username, password, done){
    try {
        const User = await userAdmin.authenticate({username, password})
        return done(null, User)
    } catch (err) {
        return done(null, false, {message: err.message})
    }
}

passport.use(
    new localStrategy ({
        usernameField: 'username',
        passwordField: 'password'
    }, authenticate)
)


passport.serializeUser(
    (user, done) => done(null, user.id)
)


passport.deserializeUser(async(id, done) => {
    const User = await userAdmin.findByPk(id)

    return done(null, User)
}
    
)

// passport.deserializeUser(async (id, done) => {
// const User = await userAdmin.findByPk(id)
// try {
//     return done(null, User)
// } catch (error) {
//     return done(null, false, {error})
// }

// });
// passport.deserializeUser(
// async (id, done) => done(null, await userAdmin.findByPk(id))
// )



module.exports = passport;


